package roboSoc.UI;


import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JFrame;

import roboSoc.Controller.IFrmPrincipal;
import roboSoc.Core.IObservador;
import roboSoc.Core.Partido;
import roboSoc.Core.RoboPartido;
import roboSoc.Core.RobotDTO;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.Font;

public class FrmPrincipal implements IObservador, IFrmPrincipal {
	private JFrame frame;
	private PnlCancha _base;
	private RoboPartido _partido;
	
	private ArrayList<RobotDTO> _equipoLocal;
	private ArrayList<PnlRobot> _panelLocal;

	private ArrayList<RobotDTO> _equipoVisitante;
	private ArrayList<PnlRobot> _panelVisitante;
	
	JLabel _lblEstado;
	
	public FrmPrincipal(RoboPartido partido) {
		this._partido=partido;
		
		this.initialize();
		this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 1100,720);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this._base = new PnlCancha();
		frame.getContentPane().add(_base);
		this._base.setSize(frame.getWidth(), frame.getHeight());
		
		this._lblEstado = new JLabel("");
		this._lblEstado.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		this._lblEstado.setForeground(Color.RED);
		this._lblEstado.setBounds(24, 656, 1039, 14);
		_base.add(this._lblEstado);
		
		inicializarEquipos();
	}

	private void inicializarEquipos()
	{
		//System.out.print("genera paneles de jugadores...");
		RoboPartido partido = (RoboPartido) this._partido;
		
		this._equipoLocal=partido.equipoLocal();
		this._panelLocal=new ArrayList<PnlRobot>();
		
		for(RobotDTO r:this._equipoLocal)
		{
			PnlRobot panel = new PnlRobot(Color.BLUE);
			this._panelLocal.add(panel);
			this._base.add(panel);
			panel.setNumero(this._panelLocal.size());
			panel.setLocation((int)r.get_posX(), (int) r.get_posY());	
		}
		
		this._equipoVisitante=partido.equipoVisitante();
		this._panelVisitante=new ArrayList<PnlRobot>();
	
		for(RobotDTO r:this._equipoVisitante)
		{
			PnlRobot panel = new PnlRobot(Color.RED);
			this._panelVisitante.add(panel);
			this._base.add(panel);
			panel.setNumero(this._panelVisitante.size());
			panel.setLocation((int)r.get_posX(), (int) r.get_posY());
		}
		
	}
	
	private void actualizarPosiciones ()
	{
		RoboPartido partido = (RoboPartido) this._partido;
	
		this._equipoLocal=partido.equipoLocal();
		this._equipoVisitante=partido.equipoVisitante();
	}
	
	private void posicionarRobots()
	{
		for(int x=0; x<this._equipoLocal.size();x++)
		{
			PnlRobot panel = this._panelLocal.get(x);
			RobotDTO robot = this._equipoLocal.get(x);
			
			panel.setLocation((int)robot.get_posX(), (int) robot.get_posY());

		}
		
		for(int x=0; x<this._equipoVisitante.size();x++)
		{
			PnlRobot panel = this._panelVisitante.get(x);
			RobotDTO robot = this._equipoVisitante.get(x);
			
			panel.setLocation((int)robot.get_posX(), (int) robot.get_posY());
		}
		
	}
	
	public void setTitle(String title)
	{
		this.frame.setTitle("UNGS Futbol de Robots" + title);
	}
	
	public void setEstado(String msj)
	{
		this._lblEstado.setText(msj);
	}
	
	
	@Override
	public void actualizar() {
		this.actualizarPosiciones ();
		this.posicionarRobots();
		this.setEstado(this._partido.estadoServicios());
		this._base.repaint();
	}

	@Override
	public void actualizarVista() {
		this.actualizar();
	}
}
