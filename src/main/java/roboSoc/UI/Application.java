package roboSoc.UI;

import roboSoc.Controller.CtrlPrincipal;
import roboSoc.Core.BuscadorComponentesJAR;
import roboSoc.Core.CircuitBreakerPosicionador;
import roboSoc.Core.GeneradorComponentesPartido;
import roboSoc.Core.IBuscadorComponentes;
import roboSoc.Core.ICircuitBreaker;
import roboSoc.Core.IGeneradorComponentes;
import roboSoc.Core.IPosicionadorRemoto;
import roboSoc.Core.RoboPartido;

public class Application {
	
	public static void main(String[] args) {
		
		IBuscadorComponentes buscador = new BuscadorComponentesJAR("c:\\roboSoc\\robosoc-pos.jar");
		IGeneradorComponentes componentes = new GeneradorComponentesPartido(buscador);
		
		IPosicionadorRemoto pos = (IPosicionadorRemoto) componentes.generarInstancia("roboSoc.Core.IPosicionadorRemoto");
		ICircuitBreaker CB = new CircuitBreakerPosicionador(pos, 5, 5);
		
		RoboPartido partido = new RoboPartido(5, CB);
		roboSoc.UI.FrmPrincipal window = new roboSoc.UI.FrmPrincipal(partido);
		CtrlPrincipal controlador = new CtrlPrincipal(window, partido);
				
		partido.agregarObservador(window);
		partido.agregarObservador(controlador);

		if(pos!=null)
			window.setTitle(" - Posicionador: " + pos.getClass().getName());
		else
			window.setTitle(" - No hay posicionadores disponibles");
		
		//Posicion posi=(Posicion) pos.getPosicion(1);
		//partido.actualizarPosiciones();
		
		//new Thread(partido).start();
		
		while (true) {			
			partido.actualizarPosiciones();
			try{ 
				Thread.sleep(500); 
			} catch(InterruptedException e ) {}; 
			
		}
		
	/*	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {				
					
					//new Thread(partido).start();
					partido.actualizarPosiciones();
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
*/
	}

}
