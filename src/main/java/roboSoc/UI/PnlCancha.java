package roboSoc.UI;


import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class PnlCancha extends JPanel {
	private static final long serialVersionUID = 1L;

	Image _imagen;
	
	public PnlCancha() {
		this.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		setLayout(null);
		
		_imagen = new ImageIcon(getClass().getResource("img/cancha.jpg")).getImage();
				
	}

	@Override
    public void paint(Graphics g) {
        g.drawImage(_imagen, 0, 0, getWidth(), getHeight(),
                        this);
 
        setOpaque(false);
        super.paint(g);
    }

}
