package roboSoc.UI;


import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Label;

import javax.swing.border.LineBorder;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;

public class PnlRobot extends JPanel {

	JLabel _leyenda;
	/**
	 * Create the panel.
	 */
	public PnlRobot() {
		setBorder(new LineBorder(new Color(0, 0, 0), 4));
		setBackground(Color.GREEN);
		this.setSize(40, 40);
		setLayout(new BorderLayout(0, 0));
		
	}
	
	public PnlRobot(Color color) {
		setBorder(new LineBorder(new Color(0, 0, 0), 4));
		setBackground(color);
		this.setSize(40, 40);
		setLayout(new BorderLayout(0, 0));
		


	}
	
	public void setNumero(int numero)
	{
		this._leyenda = new JLabel("");
		this._leyenda.setFont(new Font("Tahoma", Font.BOLD, 25));
		this.add(this._leyenda, BorderLayout.CENTER);
		
		this._leyenda.setText(String.valueOf(numero));
		//this._leyenda.setAlignment(CENTER);;
	}

}
